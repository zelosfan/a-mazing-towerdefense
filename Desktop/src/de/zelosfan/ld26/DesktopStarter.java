package de.zelosfan.ld26;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 04:32
 */
public class DesktopStarter {

    public static void main(String[] args) {
        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
        configuration.title = "TowerDefense";
        configuration.useGL20 = true;
        configuration.width = 800;
        configuration.height = 600;
        configuration.resizable = false;
        new LwjglApplication(new Main(), configuration);
    }
}
