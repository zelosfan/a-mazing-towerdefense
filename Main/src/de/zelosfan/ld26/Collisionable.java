package de.zelosfan.ld26;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 06:16
 */
public interface Collisionable {
    Rectangle getCollisionBox();
    void collidesWith(Object collidedObj);
    Vector2 getCenterPoint();
}
