package de.zelosfan.ld26;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 05:04
 */
public enum GameState {
    MainMenu, MainMenu_DEATH, MainMenu_WIN, Ingame_Fighting, Ingame_NoAction;
}
