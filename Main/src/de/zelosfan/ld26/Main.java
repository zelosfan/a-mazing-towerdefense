package de.zelosfan.ld26;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.entity.Monstertyp;
import de.zelosfan.ld26.entity.Player;
import de.zelosfan.ld26.input.InputManager;
import de.zelosfan.ld26.turrets.Turret;
import de.zelosfan.ld26.turrets.Turrettyp;
import de.zelosfan.ld26.world.Tiletyp;
import de.zelosfan.ld26.world.World;

import java.util.HashMap;
import java.util.Iterator;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 04:31
 */
public class Main extends Game{

    public static final boolean DEBUGMODE = false;

    public static HashMap<String, Texture> textureHashMap;
    public static HashMap<String, Tiletyp> tiletypHashMap;
    public static HashMap<String, Turrettyp> turrettypHashMap;
    public static HashMap<String, Monstertyp> monstertypHashMap;
    public static HashMap<String, BitmapFont> bitmapFontHashMap;
    public static HashMap<String, Sound> soundHashMap;
    public static HashMap<String, Pixmap> mapHashMap;

    public static GameState gameState = GameState.MainMenu;

    public static Long tickcount = 1L;

    public static World world;
    public static Player player;
    private SpriteBatch spriteBatch;
    private boolean alternativeTicksystem = false;
    private float delta = 0f;

    public static final int startMoney = 140;
    public static int money = startMoney;

    public static int currentMap = 1;
    public static final int mapCount = 5;

    public static Turrettyp selectedTowerTyp;
    public static Iterator<Turrettyp> selectedTurretTyp;

//    public static int TPS = 100;
    public static float tts = 0.01f;


    @Override
    public void create() {
        if (DEBUGMODE) {
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        } else {
            Gdx.app.setLogLevel(Application.LOG_ERROR);
        }

        loadMaps();
        loadBitmapfonts();
        loadTextures();
        loadTiletypes();
        loadTurrettypes();
        loadMonstertypes();
        loadSounds();

        selectedTurretTyp = turrettypHashMap.values().iterator();
        selectedTowerTyp = selectedTurretTyp.next();

        Gdx.input.setInputProcessor(new InputManager(this));

//        world = new World(new Vector2((Gdx.graphics.getWidth() / World.TILESIZE), (Gdx.graphics.getHeight() / World.TILESIZE)));

        world = new World(mapHashMap.get("map1"));
        spriteBatch = new SpriteBatch();


        player = new Player(World.playerSpawn);
        world.addEntity(player);


//        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
//            Timer.schedule(new Timer.Task() {
//                @Override
//                public void run() {
//                    tick();
//                }
//            }, 0, 0.01f);
//
//        } else {
            alternativeTicksystem = true;
//       }


    }

    public static void switchMap(int mapNr) {
        money += world.getMoneyFromTurrets();
        world = new World(mapHashMap.get("map" + mapNr));
        player = new Player(World.playerSpawn);
        world.addEntity(player);
        gameState = GameState.Ingame_Fighting;
        loadWave(mapNr);
    }

    public static void nextMap() {
        if (currentMap < mapCount) {
            currentMap += 1;
            switchMap(currentMap);
        } else {
            soundHashMap.get("success").play(0.5f);
            gameState = GameState.MainMenu_WIN;
        }
    }

    public static void gameOver() {
        gameState = GameState.MainMenu_DEATH;
//        player = new Player(World.playerSpawn);
//        world.addEntity(player);
    }

    public static void loadWave(int nr){

        switch(nr) {
            case 1:
                world.addToSpawn(monstertypHashMap.get("mob1"), 10, 600);
                world.addToSpawn(monstertypHashMap.get("mob2"), 4, 1000);
              //  world.addToSpawn(monstertypHashMap.get("boss1"), 2);
                break;

            case 2:
                world.addToSpawn(monstertypHashMap.get("mob1"), 6, 400);
                world.addToSpawn(monstertypHashMap.get("mob2"), 4, 900);
                world.addToSpawn(monstertypHashMap.get("boss1"), 1, 2000);
                break;

            case 3:
                world.addToSpawn(monstertypHashMap.get("mob1"), 10, 300);
                world.addToSpawn(monstertypHashMap.get("mob2"), 4, 500);
                world.addToSpawn(monstertypHashMap.get("mob1"), 2, 200);
                world.addToSpawn(monstertypHashMap.get("boss1"), 1, 1600);
                break;

            case 4:
                world.addToSpawn(monstertypHashMap.get("mob2"), 4, 500);
                world.addToSpawn(monstertypHashMap.get("mob1"), 2, 200);
                world.addToSpawn(monstertypHashMap.get("mob3"), 2, 500);
                world.addToSpawn(monstertypHashMap.get("boss1"), 2, 3000);
                break;

            case 5:
                world.addToSpawn(monstertypHashMap.get("mob2"), 4, 500);
                world.addToSpawn(monstertypHashMap.get("mob1"), 2, 200);
                world.addToSpawn(monstertypHashMap.get("mob3"), 2, 500);
                world.addToSpawn(monstertypHashMap.get("boss2"), 1, 3000);
                break;


            default:
                break;


        }


    }

    public void loadMaps() {
        mapHashMap = new HashMap<>();



        for (int i = 1; i <= mapCount; i++) {
            mapHashMap.put("map" + i, new Pixmap(Gdx.files.getFileHandle("maps/map" + i + ".png", Files.FileType.Internal)));
        }
    }

    public void loadSounds() {
        soundHashMap = new HashMap<>();

        soundHashMap.put("hit", Gdx.audio.newSound(Gdx.files.getFileHandle("sfx/hit.wav", Files.FileType.Internal)));
        soundHashMap.put("explosion", Gdx.audio.newSound(Gdx.files.getFileHandle("sfx/explosion.wav", Files.FileType.Internal)));
        soundHashMap.put("smallexplosion", Gdx.audio.newSound(Gdx.files.getFileHandle("sfx/smallexplosion.wav", Files.FileType.Internal)));
        soundHashMap.put("success", Gdx.audio.newSound(Gdx.files.getFileHandle("sfx/success.wav", Files.FileType.Internal)));
    }

    public void tick() {
        tickcount += 1;
        if (tickcount == Long.MAX_VALUE) {
            tickcount = 1L;
        }

        world.tick();

        if (world.allMonsterSpawned() && world.allMonsterCleared()) {
            world.showFalldown();

            if (gameState == GameState.Ingame_Fighting) {
                soundHashMap.get("success").play(0.5f);
                gameState = GameState.Ingame_NoAction;
            }


        }
    }

    public boolean input(int keycode, boolean pressed) {
        if (gameState == GameState.MainMenu || gameState == GameState.MainMenu_DEATH || gameState == GameState.MainMenu_WIN) {
            switchMap(1);
            money = startMoney;
            gameState = GameState.Ingame_Fighting;
        } else {
            if (keycode == Input.Keys.SPACE && pressed) {
                World.spawnCooldown = 0;
            }

//            if (keycode == Input.Keys.F1) {
//                world.addToSpawn(monstertypHashMap.get("test1"), 1);
//            }

            if (DEBUGMODE) {
                if (keycode == Input.Keys.F12) {
                    if (pressed) {
                        tts = 0.0025f;
                    } else {
                        tts = 0.01f;
                    }
                }
            }

            if (keycode == Input.Keys.W || keycode == Input.Keys.A || keycode == Input.Keys.S || keycode == Input.Keys.D) {
                return player.input(keycode, pressed);
            }

            if (pressed) {
                switch (keycode) {
                    case Input.Keys.UP:
                            buildTower(selectedTowerTyp, new Vector2(player.getTilePosition().x, player.getTilePosition().y + 1));
                            break;
                    case Input.Keys.LEFT:
                            buildTower(selectedTowerTyp, new Vector2(player.getTilePosition().x - 1, player.getTilePosition().y));
                            break;
                    case Input.Keys.RIGHT:
                            buildTower(selectedTowerTyp, new Vector2(player.getTilePosition().x + 1, player.getTilePosition().y));
                            break;
                    case Input.Keys.DOWN:
                            buildTower(selectedTowerTyp, new Vector2(player.getTilePosition().x, player.getTilePosition().y - 1));
                            break;
                }

                if (DEBUGMODE) {
                    switch (keycode) {
                        case Input.Keys.NUM_1:
                            switchMap(1);
                            break;
                        case Input.Keys.NUM_2:
                            switchMap(2);
                            break;
                        case Input.Keys.NUM_3:
                            switchMap(3);
                            break;
                        case Input.Keys.NUM_4:
                            switchMap(4);
                            break;
                        case Input.Keys.NUM_5:
                            switchMap(5);
                            break;

                    }
                }
            }


            if (keycode == Input.Keys.TAB && pressed) {
                if (selectedTurretTyp.hasNext()) {
                    selectedTowerTyp = selectedTurretTyp.next();
                } else {
                    selectedTurretTyp = turrettypHashMap.values().iterator();
                    selectedTowerTyp = selectedTurretTyp.next();
                }
            }
            }
        return false;
    }

    public boolean input(int mouseX, int mouseY) {
        if (DEBUGMODE || Gdx.app.getType() == Application.ApplicationType.Android) {
            buildTower(selectedTowerTyp, new Vector2(mouseX / World.TILESIZE,(Gdx.graphics.getHeight() - mouseY) / World.TILESIZE));
//        world.addTurret(new Turret(selectedTowerTyp, new Vector2(mouseX / World.TILESIZE,(Gdx.graphics.getHeight() - mouseY) / World.TILESIZE)));
            return true;
        }
        return false;
    }

    public boolean buildTower(Turrettyp turrettyp, Vector2 positionTXTY) {
        if (turrettyp.cost <= money) {
            if ((!world.getTile((int) positionTXTY.x,(int) positionTXTY.y).isWalkable() || world.getTurret(positionTXTY) != null) || world.isEntityOnField((int) positionTXTY.x, (int) positionTXTY.y)) {
                return false;
            }

            money -= turrettyp.cost;

            world.addTurret(new Turret(selectedTowerTyp, positionTXTY));

            return true;

        }
        return false;

    }

    private void loadMonstertypes() {
        monstertypHashMap = new HashMap<>();

        monstertypHashMap.put("mob1", new Monstertyp("mob1", "monster", new Vector2(0.6f, 0.6f), new Vector2(0.4f, 0.4f), new Vector2(0.01f, 0.01f), 1, 2, 4));
        monstertypHashMap.put("mob2", new Monstertyp("mob2", "red", new Vector2(0.6f, 0.6f), new Vector2(0.4f, 0.4f), new Vector2(0.01f, 0.01f), 1, 3, 8));
        monstertypHashMap.put("mob3", new Monstertyp("mob3", "red", new Vector2(0.6f, 0.6f), new Vector2(0.3f, 0.3f), new Vector2(0.01f, 0.01f), 2, 3, 12));

        monstertypHashMap.put("boss1", new Monstertyp("boss1", "boss1", new Vector2(0.6f, 0.6f), new Vector2(0.2f, 0.2f), new Vector2(0.01f, 0.01f), 2, 20, 26));
        monstertypHashMap.put("boss2", new Monstertyp("boss2", "boss2", new Vector2(0.6f, 0.6f), new Vector2(0.14f, 0.14f), new Vector2(0.01f, 0.01f), 3, 20, 46));
    }

    private void loadBitmapfonts() {
        bitmapFontHashMap = new HashMap<>();

        bitmapFontHashMap.put("debug", new BitmapFont(Gdx.files.getFileHandle("debug.fnt", Files.FileType.Internal), false));
    }

    private void loadTurrettypes() {
        turrettypHashMap = new HashMap<>();

        turrettypHashMap.put("turret1", new Turrettyp("turret", 1, 100, 150, 5, "bullet", new Vector2(0.2f, 0.2f), 4f, 0, 1, 15));
        turrettypHashMap.put("turret2", new Turrettyp("turret2", 2, 200, 200, 8, "bullet", new Vector2(0.2f, 0.2f), 3f, 0, 1, 30));
        turrettypHashMap.put("turret3", new Turrettyp("turret3", 1, 50, 250, 5, "bullet", new Vector2(0.1f, 0.1f), 8f, 0, 1, 40));
        turrettypHashMap.put("turret4", new Turrettyp("turret4", 4, 250, 200, 10, "bullet", new Vector2(0.1f, 0.1f), 2f, 0, 1, 60));
        turrettypHashMap.put("turret5", new Turrettyp("turret5", 2, 100, 250, 20, "bullet", new Vector2(0.1f, 0.1f), 6f, 0, 1, 40));

        turrettypHashMap.put("wall", new Turrettyp("wallturret", 0, 999999, 0, 40, "bullet", new Vector2(0.1f, 0.1f), 6f, 0, 1, 30));
        turrettypHashMap.put("wall2", new Turrettyp("wallturret2", 0, 999999, 0, 160, "bullet", new Vector2(0.1f, 0.1f), 6f, 0, 1, 100));
    }


    private static void loadTextures() {
        textureHashMap = new HashMap<>();

        textureHashMap.put("startscreen", new Texture("startscreen.png"));
        textureHashMap.put("deathscreen", new Texture("deathscreen.png"));
        textureHashMap.put("winscreen", new Texture("winscreen.png"));

        textureHashMap.put("turret", new Texture("turret.png"));
        textureHashMap.put("turret2", new Texture("turret2.png"));
        textureHashMap.put("turret3", new Texture("turret3.png"));
        textureHashMap.put("turret4", new Texture("turret4.png"));
        textureHashMap.put("turret5", new Texture("turret5.png"));

        textureHashMap.put("wallturret", new Texture("wallturret.png"));
        textureHashMap.put("wallturret2", new Texture("wallturret2.png"));

        textureHashMap.put("monster", new Texture("monster.png"));
        textureHashMap.put("monster2", new Texture("monster2.png"));

        textureHashMap.put("boss1", new Texture("boss1.png"));
        textureHashMap.put("boss2", new Texture("boss2.png"));

        textureHashMap.put("wall", new Texture("wall.png"));
        textureHashMap.put("empty", new Texture("empty.png"));

        textureHashMap.put("player", new Texture("player.png"));
        textureHashMap.put("spawn", new Texture("spawn.png"));
        textureHashMap.put("bullet", new Texture("bullet.png"));
        textureHashMap.put("playerspawn", new Texture("playerspawn.png"));
        textureHashMap.put("falldown", new Texture("falldown.png"));
        textureHashMap.put("red", new Texture("red.png"));




        textureHashMap.put("uiSelector", new Texture("ui_selector.png"));
        textureHashMap.put("money", new Texture("money.png"));
        textureHashMap.put("uiMoney", new Texture("ui_money.png"));
        textureHashMap.put("sword", new Texture("sword.png"));
        textureHashMap.put("clock", new Texture("clock.png"));
        textureHashMap.put("heart", new Texture("heart.png"));
        textureHashMap.put("shield", new Texture("shield.png"));

    }

    private void loadTiletypes() {
        tiletypHashMap = new HashMap<>();

        tiletypHashMap.put("empty", new Tiletyp("empty", "empty", true));
        tiletypHashMap.put("falldown", new Tiletyp("falldown", "falldown", true));
        tiletypHashMap.put("spawn", new Tiletyp("spawn", "spawn", true));
        tiletypHashMap.put("wall", new Tiletyp("wall", "wall", false));
        tiletypHashMap.put("playerspawn", new Tiletyp("playerspawn", "playerspawn", true));
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void render() {

        Gdx.graphics.getGL20().glClearColor( 0, 0, 0, 1 );
        Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );



        if (alternativeTicksystem && (gameState == GameState.Ingame_Fighting || gameState == GameState.Ingame_NoAction)) {
            delta += Gdx.graphics.getDeltaTime();

//            float dts = 10 / TPS;

//            Gdx.app.error("", "" +tts);

            int deltaC = (int) (delta / tts);

            if (deltaC > 0) {
                for (int i = 0; i < deltaC; i++) {
                    tick();
                }
                delta -= deltaC * tts;
            }
        }

        spriteBatch.begin();

        if (gameState == GameState.MainMenu || gameState == GameState.MainMenu_DEATH || gameState == GameState.MainMenu_WIN) {
            spriteBatch.draw(textureHashMap.get("startscreen"), 0, 0);

            if (gameState == GameState.MainMenu_DEATH) {
                spriteBatch.draw(textureHashMap.get("deathscreen"), Gdx.graphics.getWidth() - 344, 240);
            }
            if (gameState == GameState.MainMenu_WIN) {
                spriteBatch.draw(textureHashMap.get("winscreen"), Gdx.graphics.getWidth() - 344, 240);
            }
        }

        if (gameState == GameState.Ingame_Fighting || gameState == GameState.Ingame_NoAction) {
            world.render(spriteBatch);
            renderUI(spriteBatch);
            if (DEBUGMODE) {
       //         bitmapFontHashMap.get("debug").drawMultiLine(spriteBatch, "FPS:" + Gdx.graphics.getFramesPerSecond() + "\n RPS:" + spriteBatch.renderCalls, 0, Gdx.graphics.getHeight());
            }
        }

        spriteBatch.end();
    }

    public void renderUI(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureHashMap.get("uiSelector"), Gdx.graphics.getWidth() - 128, 0, 128, 128);
        spriteBatch.draw(textureHashMap.get(selectedTowerTyp.getTexture()), Gdx.graphics.getWidth() - 60, 55, 32, 32);


        spriteBatch.draw(textureHashMap.get("money"), Gdx.graphics.getWidth() - 104, 25, 26, 26 );
        bitmapFontHashMap.get("debug").draw(spriteBatch, "" + selectedTowerTyp.cost,  Gdx.graphics.getWidth() - 80, 45);

        spriteBatch.draw(textureHashMap.get("clock"), Gdx.graphics.getWidth() - 104, 0, 26, 26 );

        int cd = (int) ((float) (selectedTowerTyp.cooldown) / 10);
        if (cd > 9999) {
            cd = 0;
        }
        bitmapFontHashMap.get("debug").draw(spriteBatch, "" + (cd),  Gdx.graphics.getWidth() - 80, 20);

        spriteBatch.draw(textureHashMap.get("sword"), Gdx.graphics.getWidth() - 50, 0, 26, 26);
        bitmapFontHashMap.get("debug").draw(spriteBatch, "" + selectedTowerTyp.damage,  Gdx.graphics.getWidth() - 24, 20);

        spriteBatch.draw(textureHashMap.get("shield"), Gdx.graphics.getWidth() - 48, 25, 24, 24 );
        bitmapFontHashMap.get("debug").draw(spriteBatch, "" + selectedTowerTyp.maxHealth,  Gdx.graphics.getWidth() - 22, 45);



        spriteBatch.draw(textureHashMap.get("uiMoney"), Gdx.graphics.getWidth() - 128, Gdx.graphics.getHeight() - 64);
        spriteBatch.draw(textureHashMap.get("money"), Gdx.graphics.getWidth() - 100, Gdx.graphics.getHeight() - 32, 32, 32 );
        bitmapFontHashMap.get("debug").draw(spriteBatch, ""+money, Gdx.graphics.getWidth() - 70, Gdx.graphics.getHeight() - 8);

        if (World.spawnCooldown > 0) {
            spriteBatch.draw(textureHashMap.get("uiMoney"), 0, Gdx.graphics.getHeight() - 64, 128, 64, 0, 0, 128, 64, true, false);
            spriteBatch.draw(textureHashMap.get("clock"),  20, Gdx.graphics.getHeight() - 32, 32, 32 );
            bitmapFontHashMap.get("debug").draw(spriteBatch, ""+World.spawnCooldown / 100,  60, Gdx.graphics.getHeight() - 8);
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }


}
