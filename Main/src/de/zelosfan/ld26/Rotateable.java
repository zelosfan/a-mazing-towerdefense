package de.zelosfan.ld26;

import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 15:18
 */
public interface Rotateable {
    Vector2 getCenterPoint();
    float getRotationAngle();
}
