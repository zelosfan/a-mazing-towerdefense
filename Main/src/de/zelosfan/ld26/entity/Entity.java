package de.zelosfan.ld26.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Collisionable;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.world.Renderable;
import de.zelosfan.ld26.world.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 05:36
 */
public abstract class Entity implements Renderable, Collisionable{

    protected Vector2 position;
    protected Vector2 speed;
    protected Entitytyp entitytyp;
    protected int health;
    protected int maxhealth;
    public Pixmap healthPixmap;

    private boolean remove = false;

    public Entity(Entitytyp enttyp, final Vector2 positionTXTY) {
        entitytyp = enttyp;
        position = new Vector2(positionTXTY.x * World.TILESIZE + getSize().x / 4, positionTXTY.y * World.TILESIZE + getSize().y / 4);
        speed = new Vector2(0, 0);

       maxhealth = 10;
       health = 10;


       calcHealthPix();
    }

    public void calcHealthPix() {
        healthPixmap = new Pixmap( (int) getSize().x, (int) getSize().y, Pixmap.Format.RGBA8888);
        int width = (int) getSize().x;
        int height = (int) getSize().y;
        Main.textureHashMap.get(getTexture()).getTextureData().prepare();
        int color = Main.textureHashMap.get(getTexture()).getTextureData().consumePixmap().getPixel(width / 2, 0);

        int border = 2;

        healthPixmap.setColor(color);
        int h = (int) (height * (float) health / (float) maxhealth) - border * 2;
        if (h < 0 ) {
            h = 0;
        }

        healthPixmap.fillRectangle(border, border, width - border * 2, h);
    }

    public void deductHealth(int val) {

        health -= val;
        calcHealthPix();
        if (health <= 0) {
            Main.soundHashMap.get("smallexplosion").play(0.5f);
            remove = true;
        } else {
            Main.soundHashMap.get("hit").play(0.5f);
        }
    }

    public void setHealth(int health) {
        this.health = health;
        calcHealthPix();
    }

    public Boolean getsRemoved() {
        return remove;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void tick() {

    }

    public Vector2 getTilePosition() {
        return new Vector2((int) getCenterPoint().x / World.TILESIZE, (int) getCenterPoint().y / World.TILESIZE);
    }


    public boolean move(Vector2 dir) {

        if (dir.x != 0 || dir.y != 0) {
            final Vector2 acc = entitytyp.accelerationSpeed;

            speed.add(new Vector2(acc.y * dir.x, acc.y * dir.y));
            if (Math.abs(speed.x) > entitytyp.maxSpeed.x) {
                speed.x = entitytyp.maxSpeed.x * dir.x;

            }
            if (Math.abs(speed.y) > entitytyp.maxSpeed.y) {
                speed.y = entitytyp.maxSpeed.y * dir.y;
            }

        }
        float baseResistance = 0.004f;

        if (speed.x > 0) {
            speed.x -= baseResistance;
            if (speed.x < 0) {
                speed.x = 0;
            }
        }
        if (speed.x < 0) {
            speed.x += baseResistance;
            if (speed.x > 0) {
                speed.x = 0;
            }
        }
        if (speed.y > 0) {
            speed.y -= baseResistance;
            if (speed.y < 0) {
                speed.y = 0;
            }
        }
        if (speed.y < 0) {
            speed.y += baseResistance;
            if (speed.y > 0) {
                speed.y = 0;
            }
        }

        position.add(speed);

        return true;
    }

    @Override
    public String getTexture() {
        return entitytyp.getTexture();
    }

    @Override
    public Vector2 getRenderPosition() {
        return position;
    }

    @Override
    public Vector2 getSize() {
        return new Vector2(entitytyp.getSize().x * World.TILESIZE, entitytyp.getSize().y * World.TILESIZE);
    }

    @Override
    public Rectangle getCollisionBox() {
        return new Rectangle(position.x, position.y, getSize().x, getSize().y);
    }

    @Override
    public abstract void collidesWith(Object collidedObj);
}
