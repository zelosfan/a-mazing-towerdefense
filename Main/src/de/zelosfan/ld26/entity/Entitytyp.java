package de.zelosfan.ld26.entity;

import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 05:41
 */
public class Entitytyp {

    private String texture;
    public final String name;
    private Vector2 size;
    public final Vector2 maxSpeed;
    public final Vector2 accelerationSpeed;

    public Entitytyp(String name, String tex, Vector2 size, Vector2 maxSpeed, Vector2 accelerationSpeed) {
        this.name = name;
        this.texture = tex;
        this.size = size;
        this.maxSpeed = maxSpeed;
        this.accelerationSpeed = accelerationSpeed;
    }

    public String getTexture() {
        return texture;
    }

    public Vector2 getSize() {
        return size;
    }
}
