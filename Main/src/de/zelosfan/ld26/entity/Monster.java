package de.zelosfan.ld26.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.projectile.Projectile;
import de.zelosfan.ld26.turrets.Turret;
import de.zelosfan.ld26.world.Tile;
import de.zelosfan.ld26.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 05:57
 */
public class Monster extends Entity{

    protected Queue<Vector2> waypoints = new LinkedList<>();


    @Override
    public void collidesWith(Object collidedObj) {
        if (collidedObj instanceof Player) {
            Player player = (Player) collidedObj;

            player.getsTouchedByEnemy(this);
//            Gdx.app.debug("c", "mirror");


            if (getCenterPoint().x < player.getCenterPoint().x) {
                   speed.x = -0.4f;
            } else if (getCenterPoint().x > player.getCenterPoint().x) {
                    speed.x = 0.4f;
            }

            if (getCenterPoint().y < player.getCenterPoint().y) {
                    speed.y = -0.4f;
            } else if (getCenterPoint().y > player.getCenterPoint().y) {
                    speed.y = 0.4f;
            }

        }
        if (collidedObj instanceof Tile) {
            speed.x = ((1f + Math.abs(speed.x)) * (speed.x * -1) );
            speed.y = ((1f + Math.abs(speed.y)) * (speed.y * -1) );
        }

        if (collidedObj instanceof Turret) {
            Turret turret = (Turret) collidedObj;

            if (getCenterPoint().x < turret.getCenterPoint().x) {
                speed.x = -0.4f;
            } else if (getCenterPoint().x > turret.getCenterPoint().x){
                speed.x = 0.4f;
            }
            if (getCenterPoint().y < turret.getCenterPoint().y) {
                speed.y = -0.4f;
            } else if (getCenterPoint().y > turret.getCenterPoint().y){
                speed.y = 0.4f;
            }
//                if (getTilePosition().x < turret.getPosition().x) {
//                    speed.x = -0.4f;
//                } else if (getTilePosition().x > turret.getPosition().x) {
//                    speed.x = 0.4f;
//                }
//
//                if (getTilePosition().y < turret.getPosition().y) {
//                    speed.y = -0.4f;
//                } else if (getTilePosition().y > turret.getPosition().y) {
//                    speed.y = 0.4f;
//                }

        }

        if (collidedObj instanceof Projectile) {
            Projectile proj = (Projectile) collidedObj;
            deductHealth(proj.turret.getTurrettyp().damage);
            proj.remove = true;
        }
    }

    @Override
    public void deductHealth(int val) {
        super.deductHealth(val);

        if (health <= 0) {
            Main.money += ((Monstertyp)entitytyp).dropMoney;
        }
    }

    @Override
    public Vector2 getCenterPoint() {
//        Gdx.app.debug("", "" + position.x + getSize().x / 2 + " - " + position.y + getSize().y / 2);
        return new Vector2(position.x + getSize().x / 2, position.y + getSize().y / 2);
    }

    public Monstertyp getEntitytyp() {
        return (Monstertyp) entitytyp;
    }

    @Override
    public void tick() {
        super.tick();

        if (waypoints != null) {
            if (waypoints.size() <= 0 || Main.tickcount % 100 == 0) {
//                Main.world.loadStage(Main.mapHashMap.get("map1"));
                waypoints = AStar(Main.player.getTilePosition());

//                Gdx.app.debug("goal", "" + Main.player.getTilePosition().x + " - " + Main.player.getTilePosition().y);
            }
        } else {
//            Main.world.loadStage(new FileHandle("./maps/map1.png"));
            waypoints = AStar(Main.player.getTilePosition());

        }

        if (waypoints != null) {
            if (waypoints.size() > 0) {
               move();
            }
        }
    }

    public LinkedList<Vector2> AStar(Vector2 goal) {
        ArrayList<Vector2> closedList = new ArrayList<>();
        LinkedList<Vector2> openList = new LinkedList<>();

        HashMap<Vector2, Vector2> came_from = new HashMap<>();

        HashMap<Vector2, Integer> g_score = new HashMap<>();
        HashMap<Vector2, Integer> f_score = new HashMap<>();

        g_score.put(getTilePosition(), 0);
        f_score.put(getTilePosition(), (int) (g_score.get(getTilePosition()) + Math.abs(getTilePosition().dst(goal))));


        openList.add(getTilePosition());

        while (openList.size() != 0) {

            Vector2 current = openList.getFirst();
            int lowestscore = f_score.get(openList.getFirst());
            for (Vector2 vector: openList) {
                if (f_score.get(vector) < lowestscore) {
                    current = vector;
                    lowestscore = f_score.get(vector);
                }
            }


            if (current.equals(goal)) {
             //   Gdx.app.debug("A*", "goal at "+ goal.x + " - " + goal.y);
                return reconstructPath(came_from, goal);
            }

            openList.remove(current);
            closedList.add(current);

            for (int i = -1; i <= 1; i++) {
                for (int l = -1; l <= 1; l++) {
                    if (current.x + i < Main.world.mapsize.x && current.y + l < Main.world.mapsize.y && current.x + i >= 0 && current.y + l >= 0) {
                        if (Main.world.getTile((int) current.x + i,(int) current.y + l).isWalkable()) {
                            if (Math.abs(i) != Math.abs(l)) {
                                Vector2 neighbor = new Vector2(current.x + i, current.y + l);
                            //    int additionalResistance = Main.world.getTile((int) neighbor.x,(int) neighbor.y).getAdditionWaycost();
                                int additionalResistance = 0;

                                if (Main.world.getTurret(neighbor) != null) {
                                    additionalResistance = -(40 - Main.world.getTurret(neighbor).getHealth());
                                }



                                int tentative_g_score = g_score.get(current) + Math.abs((int) current.dst(neighbor));



                                if (closedList.contains(neighbor)) {
//                                    if (tentative_g_score >= g_score.get(neighbor)) {
                                        continue;
//                                    }
                                }

                                if (!openList.contains(neighbor) || tentative_g_score < g_score.get(neighbor)) {
                                    came_from.put(neighbor, current);
                                    g_score.put(neighbor, tentative_g_score + additionalResistance);

                                    int heuristic =  Math.abs((int) neighbor.dst(goal));
                                    f_score.put(neighbor, g_score.get(neighbor) + heuristic  );

                                    if (!openList.contains(neighbor)) {
                                        openList.add(neighbor);
                                    }

                                }

                            }
                        } else {
                            closedList.add(new Vector2((int) current.x + i,(int) current.y + l));
                        }
                    }
                }
            }

        }
        return null;

    }

    public LinkedList<Vector2> reconstructPath(HashMap<Vector2, Vector2> came_from, Vector2 current) { //recursion ftw
        if (came_from.get(current) != null) {
            LinkedList<Vector2> p = reconstructPath(came_from, came_from.get(current));
            p.add(current);
//            Main.world.getTile((int) current.x, (int) current.y).tiletyp = Main.tiletypHashMap.get("test2");
            return p;
        } else {
            LinkedList<Vector2> p = new LinkedList<>();
            p.add(current);
            return p;
        }
    }

    public void move() {
        if (waypoints != null) {
            if (waypoints.size() > 0) {
               // waypointindex = 0;
                //if (Math.abs(Tile.getCenterPoint((int) waypoints.get(waypointindex).x,0).x - getCenterPoint().x) < Main.WAYPOINT_TOLERANCE && Math.abs(Tile.getCenterPoint(0, (int) waypoints.get(waypointindex).y).y - getCenterPoint().y) < Main.WAYPOINT_TOLERANCE) {
                if (Tile.getRectangle((int) waypoints.peek().x, (int) waypoints.peek().y).overlaps(getCollisionBox())) {
                    if (Main.world.getTurret(waypoints.peek()) == null) {
                        waypoints.poll();
                    }
                }


                Vector2 dir = new Vector2();

                if (waypoints != null) {
                    if (!waypoints.isEmpty()) {
                        if (waypoints.peek() != null) {
                            if (Tile.getCenterPoint((int) waypoints.peek().x,0).x > getCenterPoint().x) {
                                dir.x = 1;
                            } else if (Tile.getCenterPoint((int) waypoints.peek().x,0).x < getCenterPoint().x) {
                                dir.x = -1;
                            } else {
                                dir.x = 0;
                            }

                            if (Tile.getCenterPoint(0, (int) waypoints.peek().y).y > getCenterPoint().y) {
                                dir.y = 1;
                            } else if (Tile.getCenterPoint(0, (int) waypoints.peek().y).y < getCenterPoint().y) {
                                dir.y = -1;
                            } else {
                                dir.y = 0;
                            }

//                            Gdx.app.debug("DIR", dir.x + " - " + dir.y);

//                            Gdx.app.debug("WP", "" + waypoints.peek().x + " - " + waypoints.peek().y);
                            move(dir);
                        }
                    } else {
//                        Gdx.app.debug("", "x");
                        if (Main.player.getCenterPoint().x > getCenterPoint().x) {
                            dir.x = 1;
                        } else if (Main.player.getCenterPoint().x < getCenterPoint().x) {
                            dir.x = -1;
                        } else {
                            dir.x = 0;
                        }

                        if (Main.player.getCenterPoint().y > getCenterPoint().y) {
                            dir.y = 1;
                        } else if (Main.player.getCenterPoint().y < getCenterPoint().y) {
                            dir.y = -1;
                        } else {
                            dir.y = 0;
                        }

                        move(dir);

                    }
                }
            }
        }
    }

    public void moveTo(int TX, int TY) {
        waypoints.add(new Vector2(TX * World.TILESIZE, TY * World.TILESIZE));
    }

    public Monster(Monstertyp monstertyp, final Vector2 positionTXTY) {
        super(monstertyp, positionTXTY);
        maxhealth = monstertyp.maxHealth;
        health = maxhealth;
        calcHealthPix();
        waypoints = new LinkedList<>();
    }
}
