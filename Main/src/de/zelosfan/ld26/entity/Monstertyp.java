package de.zelosfan.ld26.entity;

import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 06:00
 */
public class Monstertyp extends Entitytyp {

    public final int damage;
    public final int dropMoney;
    public final int maxHealth;

    public Monstertyp(String name, String tex, Vector2 size, Vector2 maxSpeed, Vector2 accelerationSpeed, int damage, int dropMoney, int hp) {
        super(name, tex, size, maxSpeed, accelerationSpeed);
        this.damage = damage;
        this.dropMoney = dropMoney;
        this.maxHealth = hp;
    }
}
