package de.zelosfan.ld26.entity;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.turrets.Turret;
import de.zelosfan.ld26.world.Tile;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 06:40
 */
public class Player extends Entity{

    boolean w_pressed = false;
    boolean a_pressed = false;
    boolean d_pressed = false;
    boolean s_pressed = false;

    int protectionTime = 0;

    public Player(Vector2 positionTXTY) {
        super(new Entitytyp("player", "player", new Vector2(0.8f, 0.8f), new Vector2(2f, 2f), new Vector2(0.01f, 0.01f)), positionTXTY);
    }

    @Override
    public void tick() {
        super.tick();
        protectionTime -= 1;

        Vector2 dir = new Vector2(0, 0);
        if (w_pressed) {
            dir.y += 1;
        }
        if (s_pressed) {
            dir.y -= 1;
        }
        if (d_pressed) {
            dir.x += 1;
        }
        if (a_pressed) {
            dir.x -= 1;
        }

//        Gdx.app.debug("input", ""+dir.x+ "-"+dir.y);
        move(dir);
    }

    public boolean input(int keycode, boolean pressed) {   //why me no switch
        if (keycode == Input.Keys.W) {
            w_pressed = pressed;
            return true;
        }
        if (keycode == Input.Keys.A) {
            a_pressed = pressed;
            return true;
        }
        if (keycode == Input.Keys.D) {
            d_pressed = pressed;
            return true;
        }
        if (keycode == Input.Keys.S) {
            s_pressed = pressed;
            return true;
        }

        return false;
    }

    @Override
    public void collidesWith(Object collidedObj) {
        if (collidedObj instanceof Tile) {

            Tile tile = (Tile) collidedObj;

            if (tile.tiletyp.equals(Main.tiletypHashMap.get("falldown")) || (tile.tiletyp == Main.tiletypHashMap.get("falldown") )) {
//               if (Tile.getRectangle((int) tile.position.x,(int) tile.position.y).contains(this.getCollisionBox())) {

                if (Main.world.allMonsterCleared() && Main.world.allMonsterSpawned()) {
                    Main.nextMap();
                }
//                }
            }

            if (getTilePosition().x < tile.position.x) {
                speed.x = -0.2f;
            } else if (getTilePosition().x > tile.position.x) {
                speed.x = 0.2f;
            }

            if (getTilePosition().y < tile.position.y) {
                speed.y = -0.2f;
            } else if (getTilePosition().y > tile.position.y) {
                speed.y = 0.2f;
            }

        }
        if (collidedObj instanceof Turret) {
            Turret turret = (Turret) collidedObj;

            if (getTilePosition().x < turret.getPosition().x) {
                speed.x = -0.2f;
            } else if (getTilePosition().x > turret.getPosition().x) {
                speed.x = 0.2f;
            }

            if (getTilePosition().y < turret.getPosition().y) {
                speed.y = -0.2f;
            } else if (getTilePosition().y > turret.getPosition().y) {
                speed.y = 0.2f;
            }
        }

    }

    public void getsTouchedByEnemy(Monster mon) {
//        final Vector2 v = mon.speed;
//        final Vector2 ds = new Vector2(2f, 2f);
//        speed.x = ds.x * (v.x);
//        speed.y = ds.y * (v.y);
        if (protectionTime <= 0) {
            deductHealth(1);
            protectionTime = 20;
        }
        if (getCenterPoint().x < mon.getCenterPoint().x) {
            speed.x = -0.4f;
        } else if (getCenterPoint().x > mon.getCenterPoint().x) {
            speed.x = 0.4f;
        }
        if (getCenterPoint().y < mon.getCenterPoint().y) {
            speed.y = -0.4f;
        } else if (getCenterPoint().y > mon.getCenterPoint().y) {
            speed.y = 0.4f;
        }
    }

    @Override
    public Vector2 getCenterPoint() {
        return new Vector2(position.x + getSize().x / 2, position.y + getSize().y / 2);
    }

    @Override
    public void deductHealth(int val) {
        super.deductHealth(val);
    }
}
