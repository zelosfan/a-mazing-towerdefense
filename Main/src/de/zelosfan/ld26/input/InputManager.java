package de.zelosfan.ld26.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import de.zelosfan.ld26.Main;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 07:20
 */
public class InputManager implements InputProcessor {

    Main game;

    public InputManager(Main game) {
        this.game = game;
    }

    @Override
    public boolean keyDown(int keycode) {

        return game.input(keycode, true);
    }

    @Override
    public boolean keyUp(int keycode) {
        return game.input(keycode, false);
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        return game.input(screenX, screenY);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
//        if (amount == 1) {
            if (Main.selectedTurretTyp.hasNext()) {
               Main.selectedTowerTyp = Main.selectedTurretTyp.next();
            } else {
               Main.selectedTurretTyp = Main.turrettypHashMap.values().iterator();
               Main.selectedTowerTyp = Main.selectedTurretTyp.next();
            }
            return true;
//        }
//        return false;
    }
}
