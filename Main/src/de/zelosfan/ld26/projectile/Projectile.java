package de.zelosfan.ld26.projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Collisionable;
import de.zelosfan.ld26.Rotateable;
import de.zelosfan.ld26.entity.Monster;
import de.zelosfan.ld26.turrets.Turret;
import de.zelosfan.ld26.world.Renderable;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 13:01
 */
public class Projectile implements Renderable, Collisionable, Rotateable {

    public Monster target;
    public Vector2 position;
    public Vector2 speed;
    public Turret turret;

    public int lifetime = 200;

    public boolean remove = false;

    @Override
    public Vector2 getCenterPoint() {
        return new Vector2(position.x + getSize().x / 2, position.y + getSize().y / 2);
    }

    public Monster getTarget() {
        return target;
    }

    public void tick() {
        lifetime -= 1;

        if (lifetime <= 0) {
            remove = true;
        }
//       Vector2 speedaccel = new Vector2(turret.getTurrettyp().speedAcceleration, turret.getTurrettyp().speedAcceleration);
//       speedaccel.scl(getDirection());

//       speed.add(speedaccel);
       if (target != null) {
           if (!target.getsRemoved()) {
               position.add(getInterpolatedSpeed());
           } else {
               remove = true;
           }
       } else {
           remove = true;
       }

//        position.add(getDirection().scl(speed));
//        Gdx.app.debug("", position.x + " - " + position.y);
    }

    public Vector2 getDirection() {
        Vector2 dir = new Vector2();
        if (getCenterPoint().x < target.getCenterPoint().x) {
            dir.x = 1;
        } else if(getCenterPoint().x > target.getCenterPoint().x) {
            dir.x = -1;
        } else {
            dir.x = 0;
        }

        if (getCenterPoint().y < target.getCenterPoint().y) {
            dir.y = 1;
        } else if (getCenterPoint().y > target.getCenterPoint().y) {
            dir.y = -1;
        } else {
            dir.y = 0;
        }

        return dir;
    }

    public Vector2 getInterpolatedSpeed() {
        Vector2 speedx = new Vector2();

        if (Math.abs(this.getCenterPoint().x - target.getCenterPoint().x) > Math.abs(this.getCenterPoint().y - target.getCenterPoint().y)) {
            float spd = Math.abs(this.getCenterPoint().x - target.getCenterPoint().x) / speed.x;
            speedx.x = Math.abs(speed.x) * getDirection().x;
            speedx.y = (Math.abs(this.getCenterPoint().y - target.getCenterPoint().y) / spd) * getDirection().y;
        } else {
            float spd = Math.abs(this.getCenterPoint().y - target.getCenterPoint().y) / speed.y;
            speedx.y = Math.abs(speed.y) * getDirection().y;
            speedx.x = (Math.abs(this.getCenterPoint().x - target.getCenterPoint().x) / spd) * getDirection().x;
        }

        return speedx;
    }

    @Override
    public float getRotationAngle() {
//        final float deltaY = Math.abs(target.getCenterPoint().y - getCenterPoint().y);
//        final float deltaX = Math.abs(target.getCenterPoint().x - getCenterPoint().x);
//        return (float) (Math.atan2(deltaY, deltaX) * 180 / Math.PI);
//        target.getCenterPoint().angle()
//        return 0;
//          final double t = Math.atan2(target.getCenterPoint().y, target.getCenterPoint().x) - Math.atan2(getCenterPoint().y, getCenterPoint().x);
//        Gdx.app.debug("", ""+target.getCenterPoint().angle());
        return target.getCenterPoint().angle();
//        return (float) t;
    }

    public Projectile(Monster target, Turret turret, Vector2 position) {
        this.turret = turret;
        this.target = target;
        this.position = position;
        this.speed = new Vector2(turret.getTurrettyp().startSpeed, turret.getTurrettyp().startSpeed);
    }

    @Override
    public Rectangle getCollisionBox() {
        return new Rectangle(position.x, position.y, getSize().x, getSize().y);
    }

    @Override
    public void collidesWith(Object collidedObj) {
//        if (collidedObj.equals(target)) {
//            turret.onImpact(target);
//        }
        if (collidedObj instanceof Monster) {
            turret.onImpact((Monster) collidedObj);
        }
    }

    @Override
    public String getTexture() {
        return turret.getTurrettyp().bulletTexture;
    }

    @Override
    public Vector2 getRenderPosition() {
        return new Vector2(position.x, position.y);
    }

    @Override
    public Vector2 getSize() {
        return turret.getTurrettyp().bulletSize;
    }
}
