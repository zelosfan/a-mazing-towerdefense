package de.zelosfan.ld26.turrets;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Collisionable;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.Rotateable;
import de.zelosfan.ld26.entity.Monster;
import de.zelosfan.ld26.entity.Player;
import de.zelosfan.ld26.world.Renderable;
import de.zelosfan.ld26.world.World;


/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 05:11
 */
public class Turret implements Renderable, Collisionable, Rotateable {

    private Turrettyp turrettyp;
    private Vector2 position;
    private int health;
    private boolean remove = false;
    private int cooldown;
    private Monster currenttarget;
    public Pixmap healthPixmap;
    private int damageprotection;

    public Turret(Turrettyp turrettyp, Vector2 position) {
        this.turrettyp = turrettyp;
        this.position = position;
        this.cooldown = turrettyp.cooldown;
        this.health = turrettyp.maxHealth;

        calcHealthPix();
    }

    public void calcHealthPix() {
        healthPixmap = new Pixmap( (int) getSize().x, (int) getSize().y, Pixmap.Format.RGBA8888);
        int width = (int) getSize().x;
        int height = (int) getSize().y;
        Main.textureHashMap.get(getTexture()).getTextureData().prepare();
        int color = Main.textureHashMap.get(getTexture()).getTextureData().consumePixmap().getPixel(4, 0);

        int border = 2;

        healthPixmap.setColor(color);
        int h = (int) (height * (float) health / (float) turrettyp.maxHealth) - border * 2;
        if (h < 0 ) {
            h = 0;
        }

        healthPixmap.fillRectangle(border, border, width - border * 2, h);
    }

    public Vector2 getPosition() {
        return  position;
    }

    public Turrettyp getTurrettyp() {
        return turrettyp;
    }

    public void tick() {
        cooldown -= 1;
        damageprotection -= 1;

        if (cooldown < 0) {

            if (currenttarget == null) {
                Monster target = getTarget();
                if (target != null) {

                    if (onAttack(target)) {
                        currenttarget = target;
                        cooldown = turrettyp.cooldown;
                    }
                }
            } else if (currenttarget.getsRemoved()) {
                Monster target = getTarget();
                if (target != null) {

                    if (onAttack(target)) {
                        currenttarget = target;
                        cooldown = turrettyp.cooldown;
                    }
                }
            } else {
                if (onAttack(currenttarget)) {
                    cooldown = turrettyp.cooldown;
                }
            }

        }
    }

    public Monster getTarget() {
        if (!Main.world.getMonstersInRange(getRenderPosition(), turrettyp.range).isEmpty()) {
            return Main.world.getMonstersInRange(getRenderPosition(), turrettyp.range).get(0);
        } else {
            return null;
        }
    }

    public void deductHealth(int val) {
        if (damageprotection <= 0) {
            health -= val;
            Main.soundHashMap.get("hit").play(0.5f);
            if (health <= 0) {
                remove = true;
                Main.soundHashMap.get("explosion").play(0.5f);
            }
            calcHealthPix();
            damageprotection = 20;
        }
    }

    public int getHealth() {
        return health;
    }

    public boolean getsRemoved() {
        return remove;
    }

    public boolean onAttack(Monster target) {
        return turrettyp.onAttack(target, this);
    }

    public void onImpact(Monster target) {
        turrettyp.onImpact(target);
    }

    @Override
    public String getTexture() {
        return turrettyp.getTexture();
    }

    @Override
    public Vector2 getRenderPosition() {
        return new Vector2(position.x * World.TILESIZE, position.y * World.TILESIZE);
    }

    @Override
    public Vector2 getSize() {
        return World.TILESIZEV2;
    }

    @Override
    public Rectangle getCollisionBox() {
        return new Rectangle(position.x * World.TILESIZE, position.y * World.TILESIZE, World.TILESIZE, World.TILESIZE);
    }

    @Override
    public void collidesWith(Object collidedObj) {
        if (collidedObj instanceof Monster) {
            Monster mon = (Monster) collidedObj;
            deductHealth(mon.getEntitytyp().damage);
        }
        if (collidedObj instanceof Player) {

        }
    }

    @Override
    public Vector2 getCenterPoint() {
        return new Vector2(position.x * World.TILESIZE + getSize().x / 2, position.y * World.TILESIZE + getSize().y / 2);
    }

    @Override
    public float getRotationAngle() {
        return 0;
    }
}
