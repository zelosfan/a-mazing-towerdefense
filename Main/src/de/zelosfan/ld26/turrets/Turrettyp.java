package de.zelosfan.ld26.turrets;

import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.entity.Monster;
import de.zelosfan.ld26.projectile.Projectile;
import de.zelosfan.ld26.world.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 05:15
 */
public class Turrettyp {

    private String texture;
    public final int maxHealth;
    public final int cooldown;
    public final int damage;
    public final float range;
    public final String bulletTexture;
    public final Vector2 bulletSize;
    public final float startSpeed;
    public final float speedAcceleration;
    public final float maximumSpeed;
    public final int cost;

    public Turrettyp(String tex, int damage, int cooldown, float range, int maxHealth, String bulletTexture, Vector2 bulletSize, float startSpeed, float speedAcceleration, float maximumSpeed, int cost) {
        texture = tex;
        this.bulletTexture = bulletTexture;
        this.cooldown = cooldown;
        this.damage = damage;
        this.range = range;
        this.maxHealth = maxHealth;
        this.bulletSize = new Vector2(bulletSize.x * World.TILESIZE, bulletSize.y * World.TILESIZE);
        this.startSpeed = startSpeed;
        this.speedAcceleration = speedAcceleration;
        this.maximumSpeed = maximumSpeed;
        this.cost = cost;
    }

    public String getTexture() {
        return texture;
    }

    public boolean onAttack(Monster target, Turret turret) {
        Main.world.addProjectile(new Projectile(target, turret, turret.getCenterPoint()));

        return true;
    }

    public void onImpact(Monster target) {

    }
}
