package de.zelosfan.ld26.world;

import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 04:43
 */
public interface Renderable {
    String getTexture();
    Vector2 getRenderPosition();
    Vector2 getSize();
}
