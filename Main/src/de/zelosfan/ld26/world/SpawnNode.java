package de.zelosfan.ld26.world;

import de.zelosfan.ld26.entity.Monstertyp;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 28.04.13
 * Time: 00:55
 */
public class SpawnNode {

    public final Monstertyp monstertyp;
    public final int amount;
    public int left;
    public final int cooldown;

    public SpawnNode(Monstertyp monstertyp, int amount, int cooldown) {
        this.monstertyp = monstertyp;
        this.amount = amount;
        left = amount;
        this.cooldown = cooldown;

    }

    public Monstertyp spawn() {
        if (left > 0) {
            left -= 1;
            return monstertyp;
        }
        return null;
    }
}
