package de.zelosfan.ld26.world;

import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.entity.Monster;
import de.zelosfan.ld26.entity.Monstertyp;

import java.util.LinkedList;
import java.util.Queue;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 28.04.13
 * Time: 00:52
 */
public class SpawnTile extends Tile {
    Queue<SpawnNode> spawnNodes = new LinkedList<>();

   // public final int maxCooldown;
    public int cooldown;

    public SpawnTile(Tiletyp tiletyp, Vector2 position) {
        super(tiletyp, position);
    }

    @Override
    public void tick() {
        super.tick();

        if (World.spawnCooldown <= 0) {

            cooldown = cooldown - 1;
            if (cooldown <= 0) {
                if (!spawnNodes.isEmpty()) {
                    this.cooldown = spawnNodes.peek().cooldown;
                    spawn();

                } else {
    //                Main.world.
                }
            }

        }

    }

    public boolean isEmpty() {
        return spawnNodes.isEmpty();
    }

    public void spawn() {
        final Vector2 pos = position;
        Main.world.addEntity(new Monster(spawnNodes.peek().spawn(), pos));
        if (spawnNodes.peek().left <= 0) {
            spawnNodes.poll();
        }
    }

    public void addToSpawn(Monstertyp monstertyp, int amount, int cooldown) {
        spawnNodes.add(new SpawnNode(monstertyp, amount, cooldown));
    }
}
