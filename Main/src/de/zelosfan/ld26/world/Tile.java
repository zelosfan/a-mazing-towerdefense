package de.zelosfan.ld26.world;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.Main;
import org.w3c.dom.css.Rect;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 04:42
 */
public class Tile implements Renderable {

    public Tiletyp tiletyp;
    public final Vector2 position;

    public Tile(Tiletyp tiletyp, Vector2 position) {
        this.tiletyp = tiletyp;
        this.position = position;
    }

    public void tick() {

    }

    @Override
    public String getTexture() {
        return tiletyp.getTexture();
    }

    @Override
    public Vector2 getRenderPosition() {
        return new Vector2(position.x * World.TILESIZE, position.y * World.TILESIZE);
    }

    @Override
    public Vector2 getSize() {
        return World.TILESIZEV2;
    }

    public boolean isWalkable() {
        return tiletyp.isMoveable();
    }

    public int getAdditionWaycost() {
        int wc = 0;
        if (Main.world.getTurret(position) != null) {
            wc = -(20 - Main.world.getTurret(position).getHealth());
        }
        return wc;
    }

    public static Vector2 getCenterPoint(int X, int Y) {
        return new Vector2(X * World.TILESIZE + World.TILESIZE / 2, Y * World.TILESIZE + World.TILESIZE / 2);
    }

    public static Rectangle getRectangle(int X, int Y) {
        return new Rectangle(X * World.TILESIZE, Y * World.TILESIZE, World.TILESIZE, World.TILESIZE);
    }
}
