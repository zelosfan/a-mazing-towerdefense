package de.zelosfan.ld26.world;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 04:47
 */
public class Tiletyp {

    public final String texture;
    public final String name;
    private boolean moveable;


    public Tiletyp(String tex, String name, boolean moveable) {
        texture = tex;
        this.name = name;
        this.moveable = moveable;
    }

    public String getTexture() {
        return texture;
    }

    public boolean isMoveable() {
        return moveable;
    }
}
