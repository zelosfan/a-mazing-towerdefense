package de.zelosfan.ld26.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.ld26.GameState;
import de.zelosfan.ld26.Main;
import de.zelosfan.ld26.Rotateable;
import de.zelosfan.ld26.entity.Entity;
import de.zelosfan.ld26.entity.Monster;
import de.zelosfan.ld26.entity.Monstertyp;
import de.zelosfan.ld26.entity.Player;
import de.zelosfan.ld26.projectile.Projectile;
import de.zelosfan.ld26.turrets.Turret;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 27.04.13
 * Time: 04:37
 */
public class World {

    public Vector2 mapsize;
    private Tile[][] tileMap;
    private ArrayList<Turret> turrets;
    private LinkedList<Entity> entities;
    private LinkedList<Projectile> bullets;

    public static Vector2 TILECOUNT = new Vector2(20, 15);
    public static int TILESIZE = (int) (Gdx.graphics.getHeight() / TILECOUNT.y);
    public static Vector2 TILESIZEV2 = new Vector2(TILESIZE, TILESIZE);
    public static int maxSpawncooldown = 3000;

    public static Vector2 playerSpawn = new Vector2(1, 1);

    public static ArrayList<SpawnTile> spawnTiles;

    public static int spawnCooldown;

    public ArrayList<Vector2> falldown;

    public int getMoneyFromTurrets() {
        int amnt = 0;
        for (Turret turret: turrets) {
            amnt += turret.getTurrettyp().cost / 3;
        }
        return amnt;
    }

    public void addToSpawn(Monstertyp monstertyp, int amount, int spawnCooldown) {
        for (SpawnTile spawnTile: spawnTiles ) {
            spawnTile.addToSpawn(monstertyp, amount, spawnCooldown);
        }
    }

    public World(Vector2 size) {

        turrets = new ArrayList<>();
        entities = new LinkedList<>();
        bullets = new LinkedList<>();

        mapsize = size;

        tileMap= new Tile[(int) size.x][(int) size.y];

        for (int i = 0; i < (int) mapsize.x; i++) {
            for (int l = 0; l < (int) mapsize.y; l++) {
                tileMap[i][l] = new Tile(Main.tiletypHashMap.get("test"), new Vector2(i, l));
            }
        }
    }

    public World(Pixmap pix) {
        turrets = new ArrayList<>();
        entities = new LinkedList<>();
        bullets = new LinkedList<>();


        loadStage(pix);

    }

    public void reset() {
        turrets.clear();
        entities.clear();
        bullets.clear();

    }

    public ArrayList<Monster> getMonstersInRange(Vector2 position, float range) {
        ArrayList<Monster> result = new ArrayList<>();
        for (Entity entity: entities) {
            if (entity instanceof  Monster) {
                if (position.dst(entity.getPosition()) <= range) {
                    result.add((Monster) entity);
                }
            }
        }
        return result;
    }

    public void showFalldown() {
        for (Vector2 vector2: falldown) {
            tileMap[(int) vector2.x][(int) vector2.y] = new Tile(Main.tiletypHashMap.get("falldown"), vector2);
        }
    }

    public boolean allMonsterSpawned() {
        for (SpawnTile spawnTile: spawnTiles) {
            if (!spawnTile.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public boolean allMonsterCleared() {
        for (Entity entity: entities) {
            if (entity instanceof Monster) {
                return false;
            }
        }
        return true;
    }


    public void loadStage(Pixmap tex) {

       // Pixmap tex = new Pixmap(file);

        mapsize = new Vector2(tex.getWidth() , tex.getHeight());
        tileMap = new Tile[tex.getWidth()][tex.getHeight()];
        falldown = new ArrayList<>();
        spawnTiles = new ArrayList<>();

        spawnCooldown = maxSpawncooldown;

        for (int i = 0; i < tex.getWidth(); i++) {
            for (int l = 0; l < tex.getHeight(); l++) {
//                Gdx.app.debug("", ""+tex.getPixel(i, l));

                switch (tex.getPixel(i, tex.getHeight() - l - 1)) {
                    case 255:
                        tileMap[i][l] = new Tile(Main.tiletypHashMap.get("spawn"), new Vector2(i, l));
                        break;
                    case -16712449:
                        tileMap[i][l] = new Tile(Main.tiletypHashMap.get("empty"), new Vector2(i, l));
                        falldown.add( new Vector2(i, l));
                        break;
                    case -316922625:
                        tileMap[i][l] = new Tile(Main.tiletypHashMap.get("wall"), new Vector2(i, l));
                        break;
                    case 218038527:
                        SpawnTile spwn = new SpawnTile(Main.tiletypHashMap.get("spawn"), new Vector2(i, l));
                        tileMap[i][l] = spwn;
                        spawnTiles.add(spwn);
                        break;
                    case 100723711:
                        playerSpawn = new Vector2(i, l);
                        tileMap[i][l] = new Tile(Main.tiletypHashMap.get("playerspawn"), new Vector2(i, l));
                        break;
                    default:
                        tileMap[i][l] = new Tile(Main.tiletypHashMap.get("empty"), new Vector2(i, l));
                        break;

                }
            }
        }
    }

    public boolean isEntityOnField(int X, int Y) {
        for (Entity entity: entities) {
            if (entity.getTilePosition().equals(new Vector2(X, Y))) {
                return true;
            }
        }
        return false;
    }

    public void tick() {
        ArrayList<Entity> garbageEntities = new ArrayList<>();
        ArrayList<Turret> garbageTurrets = new ArrayList<>();
        ArrayList<Projectile> garbageProjectile = new ArrayList<>();

        spawnCooldown -= 1;

        for (Entity entity: entities) {
            entity.tick();

            if (entity.getsRemoved()) {
                garbageEntities.add(entity);
            }
        }
        for (Turret turret: turrets) {
            turret.tick();

            if (turret.getsRemoved()) {
                garbageTurrets.add(turret);
            }

        }
        for (Projectile projectile: bullets) {
            projectile.tick();

            if (projectile.remove) {
                garbageProjectile.add(projectile);
            }
        }

        //garbage it
        for (Entity entity: garbageEntities) {
            entities.remove(entity);
            if (entity instanceof Player) {
                Main.gameOver();
            }
        }
        for (Turret turret: garbageTurrets) {
            turrets.remove(turret);
        }
        for (Projectile projectile: garbageProjectile) {
            bullets.remove(projectile);
        }


        for (int i = 0; i < mapsize.x; i++) {
            for (int l = 0; l < mapsize.y; l++) {
                tileMap[i][l].tick();
            }
        }


        //collision
        for (Entity entity: entities) {
            for (Entity entity1: entities) {
                if (!entity.equals(entity1) && !entity1.getsRemoved()) {
                    if (entity.getCollisionBox().overlaps(entity1.getCollisionBox())) {
                        entity.collidesWith(entity1);
                    }
                }
            }

            for (Projectile projectile: bullets) {
                if (projectile.getCollisionBox().overlaps(entity.getCollisionBox())) {
                    entity.collidesWith(projectile);
                }
            }

            for (Turret turret: turrets) {
                if (Main.gameState == GameState.Ingame_Fighting && turret.getCollisionBox().overlaps(entity.getCollisionBox())) {
                    entity.collidesWith(turret);
                    turret.collidesWith(entity);
                }
            }

            for (int i = -1; i <= 1; i++) {
                for (int l = -1; l <= 1; l++) {
                    if ((int) entity.getTilePosition().x + i < this.mapsize.x && (int) entity.getTilePosition().y + l < this.mapsize.y && (int) entity.getTilePosition().x + i >= 0 && (int) entity.getTilePosition().y + l >= 0) {
                        if ( (!tileMap[(int) entity.getTilePosition().x + i][(int) entity.getTilePosition().y + l].isWalkable() || tileMap[(int) entity.getTilePosition().x + i][(int) entity.getTilePosition().y + l].tiletyp.equals(Main.tiletypHashMap.get("falldown")))) {
                            if (entity.getCollisionBox().overlaps(Tile.getRectangle((int) entity.getTilePosition().x + i, (int) entity.getTilePosition().y + l))) {
                                entity.collidesWith(tileMap[(int) entity.getTilePosition().x + i][(int) entity.getTilePosition().y + l]);
                            }
                        }
                    }
                }
            }
        }
    }

    public void addProjectile(Projectile projectile) {
        bullets.add(projectile);
    }

    public Tile getTile(int X, int Y) {
        return tileMap[X][Y];
    }

    public void render(SpriteBatch spriteBatch) {

        for (int i = 0; i < (int) mapsize.x; i++) {
            for (int l = 0; l < (int) mapsize.y; l++) {
                spriteBatch.draw(Main.textureHashMap.get(tileMap[i][l].getTexture()), tileMap[i][l].getRenderPosition().x, tileMap[i][l].getRenderPosition().y, TILESIZE, TILESIZE);
            }
        }

        for (Turret turret: turrets) {
            spriteBatch.draw(Main.textureHashMap.get(turret.getTexture()), turret.getRenderPosition().x, turret.getRenderPosition().y, 0.5f, 0.5f, TILESIZE, TILESIZE, 1, 1, ((Rotateable) turret).getRotationAngle(), 0, 0, 64, 64, false, false);
            spriteBatch.draw(new Texture(turret.healthPixmap), turret.getRenderPosition().x, turret.getRenderPosition().y, turret.getSize().x, turret.getSize().y, 0, 0, (int) turret.getSize().x, (int) turret.getSize().y, false, true);

        }

        for (Entity entity: entities) {
            spriteBatch.draw(Main.textureHashMap.get(entity.getTexture()), entity.getRenderPosition().x, entity.getRenderPosition().y, entity.getSize().x, entity.getSize().y);

            spriteBatch.draw(new Texture(entity.healthPixmap), entity.getRenderPosition().x, entity.getRenderPosition().y, entity.getSize().x, entity.getSize().y, 0, 0, (int) entity.getSize().x, (int) entity.getSize().y, false, true);
        }

        for (Projectile projectile: bullets) {
            if(projectile instanceof Rotateable) {
                spriteBatch.draw(Main.textureHashMap.get(projectile.getTexture()), projectile.getRenderPosition().x, projectile.getRenderPosition().y, 0.5f, 0.5f, projectile.getSize().x, projectile.getSize().y, 1, 1, ((Rotateable) projectile).getRotationAngle(), 0, 0, 64, 64, false, false);
            } else {
                spriteBatch.draw(Main.textureHashMap.get(projectile.getTexture()), projectile.getRenderPosition().x, projectile.getRenderPosition().y, projectile.getSize().x, projectile.getSize().y);
            }
        }

    }

    public Turret getTurret(Vector2 position) {
        //performance?
        for (Turret turret: turrets) {
            if (turret.getPosition().equals(position)) {
                return turret;
            }
        }
        return null;
    }

    public boolean addEntity(Entity entity) {
        if (!entities.contains(entity) && !entity.getsRemoved()) {
            entities.add(entity);
            return true;
        }
        return false;

    }

    public boolean addTurret(Turret turret) {
        if (getTurret(turret.getPosition()) == null && !turrets.contains(turret)) {
            turrets.add(turret);
            return true;
        }
        return false;

    }



}
